// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2017 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __GMETER_H
#define	__GMETER_H


#include <clxclient.h>


class Gmeter : public X_window
{
public:

    Gmeter (X_window *parent, int xpos, int ypos);
    ~Gmeter (void);
    Gmeter (const Gmeter&);
    Gmeter& operator=(const Gmeter&);

    void update (float v0, float v1);

    static XImage  *_scale;
    static XImage  *_imag0;
    static XImage  *_imag1;

private:

    enum { XS = 253, YS = 22, YM = 15, DY = 7 };

    void handle_event (XEvent *E);
    void expose (XExposeEvent *E);

    int     _k0;
    int     _k1;
};


#endif
