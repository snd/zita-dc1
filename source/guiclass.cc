// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2017 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include <math.h>
#include "guiclass.h"


// Simple on/off toggle.

int Pbutt1::handle_press (void)
{
    _state = (_state | 1) ^ 2;
    return PRESS;
}

int Pbutt1::handle_relse (void)
{
    _state = _state & ~1;
    return 0;
}



Rlinctl::Rlinctl (X_window   *parent,
                  X_callback *cbobj,
		  int         cbind,
                  RotaryGeom *geom,
                  int         xp,
                  int         yp,
		  int         cm,
		  int         dd,
                  double      vmin,
	          double      vmax,
	          double      vini):
RotaryCtl (parent, cbobj, cbind, geom, xp, yp),
_cm (cm),
_dd (dd),
_vmin (vmin),
_vmax (vmax),
_form (0)
{
    _count = -1;
    set_value (vini);
}

void Rlinctl::get_string (char *p, int n)
{
    if (_form) snprintf (p, n, _form, _value);
    else *p = 0;
}

void Rlinctl::set_value (double v)
{
    set_count ((int) floor (_cm * (v - _vmin) / (_vmax - _vmin) + 0.5));
    render ();
}

int Rlinctl::handle_button (void)
{
    return PRESS;
}

int Rlinctl::handle_motion (int dx, int dy)
{
    return set_count (_rcount + dx - dy);
}

int Rlinctl::handle_mwheel (int dw)
{
    if (! (_keymod & ShiftMask)) dw *= _dd;
    return set_count (_count + dw);
}

int Rlinctl::set_count (int u)
{
    if (u <   0) u=    0;
    if (u > _cm) u = _cm;
    if (u != _count)
    {
	_count = u;
	_value = _vmin + u * (_vmax - _vmin) / _cm;
	_angle = 270.0 * ((double) u / _cm - 0.5);
        return DELTA;
    }
    return 0;
}



Rlogctl::Rlogctl (X_window   *parent,
                  X_callback *cbobj,
		  int         cbind,
                  RotaryGeom *geom,
                  int         xp,
                  int         yp,
		  int         cm,
		  int         dd,
                  double      vmin,
	          double      vmax,
	          double      vini):
RotaryCtl (parent, cbobj, cbind, geom, xp, yp),
_cm (cm),
_dd (dd),
_form (0)
{
    _count = -1;
    _vmin = log (vmin);
    _vmax = log (vmax);
    set_value (vini);
}

void Rlogctl::get_string (char *p, int n)
{
    if (_form) snprintf (p, n, _form, _value);
    else *p = 0;
}

void Rlogctl::set_value (double v)
{
    set_count ((int) floor (_cm * (log (v) - _vmin) / (_vmax - _vmin) + 0.5));
    render ();
}

int Rlogctl::handle_button (void)
{
    return PRESS;
}

int Rlogctl::handle_motion (int dx, int dy)
{
    return set_count (_rcount + dx - dy);
}

int Rlogctl::handle_mwheel (int dw)
{
    if (! (_keymod & ShiftMask)) dw *= _dd;
    return set_count (_count + dw);
}

int Rlogctl::set_count (int u)
{
    if (u <   0) u=    0;
    if (u > _cm) u = _cm;
    if (u != _count)
    {
	_count = u;
	_value = exp (_vmin + u * (_vmax - _vmin) / _cm);
	_angle = 270.0 * ((double) u / _cm - 0.5);
        return DELTA;
    }
    return 0;
}



Rcratio::Rcratio (X_window   *parent,
                  X_callback *cbobj,
		  int         cbind,
                  RotaryGeom *geom,
                  int        xp,
                  int        yp):
RotaryCtl (parent, cbobj, cbind, geom, xp, yp)
{
    _count = -1;
    _lastc = 0;
    set_value (0);
}

void Rcratio::get_string (char *p, int n)
{
    *p = 0;
}

void Rcratio::set_value (double v)
{
    set_count ((int) floor (150 * v * v + 0.5));
    render ();
}

int Rcratio::handle_button (void)
{
    int k;

    if (_button == 3)
    {
	if (_count)
	{
	    k = 0;
            _lastc = _count;
	}
	else
	{
	    k = _lastc;
	    _lastc = 0;
	}
        return set_count (k);
    }
    return PRESS;
}

int Rcratio::handle_motion (int dx, int dy)
{
    return set_count (_rcount + dx - dy);
}

int Rcratio::handle_mwheel (int dw)
{
    if (! (_keymod & ShiftMask)) dw *= 4;
    return set_count (_count + dw);
}

int Rcratio::set_count (int u)
{
    if (u <   0) u =   0;
    if (u > 150) u = 150;
    if (u != _count)
    {
	_count = u;
	_value = sqrt (_count / 150.0);
	_angle = 1.8 * (_count - 75);
        return DELTA;
    }
    return 0;
}
