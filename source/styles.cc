// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2017 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include "styles.h"
#include "gmeter.h"
#include "png2img.h"


XftColor      *XftColors [NXFTCOLORS];
XftFont       *XftFonts [NXFTFONTS];

//X_textln_style tstyle1;

XImage    *parsect;
XImage    *redzita;
XImage    *hldbutt;
XImage    *ambbutt;

RotaryGeom  ipgain_geom;
RotaryGeom  thresh_geom;
RotaryGeom  cratio_geom;
RotaryGeom  attack_geom;
RotaryGeom  release_geom;


XImage *loadpng (const char *file, X_display *disp)
{
    char    s [1024];
    XImage  *img;

    sprintf (s, "%s/%s.png", SHARED, file);
    img = png2img (s, disp, 0);
    if (img) return img;
    exit (1);
}


void styles_init (X_display *disp, X_resman *xrm)
{
    XftColors [C_MAIN_BG] = disp->alloc_xftcolor (0.25f, 0.25f, 0.25f, 1.0f);
    XftColors [C_MAIN_FG] = disp->alloc_xftcolor (1.0f, 1.0f, 1.0f, 1.0f);
    XftColors [C_TEXT_BG] = disp->alloc_xftcolor (1.0f, 1.0f, 0.0f, 1.0f);
    XftColors [C_TEXT_FG] = disp->alloc_xftcolor (0.0f, 0.0f, 0.0f, 1.0f);

    XftFonts [F_TEXT] = disp->alloc_xftfont (xrm->get (".font.text", "luxi:bold::pixelsize=11"));

    hldbutt = loadpng ("hldbutt", disp);
    ambbutt = loadpng ("ambbutt", disp);
    
//    tstyle1.font = XftFonts [F_TEXT];
//    tstyle1.color.normal.bgnd = XftColors [C_TEXT_BG]->pixel;
//    tstyle1.color.normal.text = XftColors [C_TEXT_FG];

    parsect = loadpng ("parsect", disp);
    redzita = loadpng ("redzita", disp);
    Gmeter::_scale = png2img (SHARED"/hscale1.png", disp, XftColors [C_MAIN_BG]);
    Gmeter::_imag0 = png2img (SHARED"/hmeter0.png", disp, XftColors [C_MAIN_BG]);
    Gmeter::_imag1 = png2img (SHARED"/hmeter1.png", disp, XftColors [C_MAIN_BG]);

    ipgain_geom._backg = XftColors [C_MAIN_BG];
    ipgain_geom._image [0] = parsect;
    ipgain_geom._lncol [0] = 0;
    ipgain_geom._x0 = 24;
    ipgain_geom._y0 = 18;
    ipgain_geom._dx = 23;
    ipgain_geom._dy = 23;
    ipgain_geom._xref = 11.5;
    ipgain_geom._yref = 11.5;
    ipgain_geom._rad = 11;

    thresh_geom._backg = XftColors [C_MAIN_BG];
    thresh_geom._image [0] = parsect;
    thresh_geom._lncol [0] = 0;
    thresh_geom._x0 = 89;
    thresh_geom._y0 = 18;
    thresh_geom._dx = 23;
    thresh_geom._dy = 23;
    thresh_geom._xref = 11.5;
    thresh_geom._yref = 11.5;
    thresh_geom._rad = 11;

    cratio_geom._backg = XftColors [C_MAIN_BG];
    cratio_geom._image [0] = parsect;
    cratio_geom._lncol [0] = 0;
    cratio_geom._x0 = 154;
    cratio_geom._y0 = 18;
    cratio_geom._dx = 23;
    cratio_geom._dy = 23;
    cratio_geom._xref = 11.5;
    cratio_geom._yref = 11.5;
    cratio_geom._rad = 11;

    attack_geom._backg = XftColors [C_MAIN_BG];
    attack_geom._image [0] = parsect;
    attack_geom._lncol [0] = 0;
    attack_geom._x0 = 219;
    attack_geom._y0 = 33;
    attack_geom._dx = 23;
    attack_geom._dy = 23;
    attack_geom._xref = 11.5;
    attack_geom._yref = 11.5;
    attack_geom._rad = 11;

    release_geom._backg = XftColors [C_MAIN_BG];
    release_geom._image [0] = parsect;
    release_geom._lncol [0] = 0;
    release_geom._x0 = 284;
    release_geom._y0 = 33;
    release_geom._dx = 23;
    release_geom._dy = 23;
    release_geom._xref = 11.5;
    release_geom._yref = 11.5;
    release_geom._rad = 11;
}


void styles_fini (X_display *disp)
{
}
